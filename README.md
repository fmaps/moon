# moon

Moon LRO LOLA Color Shaded Relief 388m v4

* Uploaded to Mapbox as [tileset](https://docs.mapbox.com/help/glossary/tileset/)
* re-projected to Web Mercator EPSG:3857

### Data source:

https://astrogeology.usgs.gov/search/map/Moon/LMMP/LOLA-derived/Lunar_LRO_LOLA_ClrShade_Global_128ppd_v04

### Product Information:

* Map Projection Name:     Simple Cylindrical
* Latitude Type:    Planetocentric
* Longitude Direction:    Positive East
* Longitude Domain:    -180 to 180

This is a colorized shaded-relief of a original polar digital elevation model (DEM) from the Lunar Orbiter Laser Altimeter (LOLA; Smith et al., 2010), an instrument on the National Aeronautics and Space Agency (NASA) Lunar Reconnaissance Orbiter (LRO) spacecraft (Tooley et al., 2010).

The DEM is a shape map (radius) of the Moon at a resolution 100 meters per pixel (m) based on altimetry data acquired through September, 2011 by the LOLA instrument. The ground tracks were interpolated using the Generic Mapping Tools programs "surface" and "grdblend". Map values are referred to a radius of 1,737,400 m.

Legend

![](https://astropedia.astrogeology.usgs.gov/download/Moon/LMMP/LOLA-derived/ancillary/LRO_LOLA_ClrShade_Global_256ppd_legend.png)
